@extends('layouts.admin')
@section('title')
    Yönetim Ayarları
@endsection
@section('content')
    <section class="content">
        @if(session('success'))
        <div class="row">
            <div class="col-sm-12 alert alert-success">
                {{ session('success') }}
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-body">

                        <hr>
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Ad</th>
                                    <th>Soyad</th>
                                    <th>Email</th>

                                    <th>İşlemler</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>

									<td>{{$user->id}}</td>
									<td>{{$user->name}}</td>
                  <td>{{$user->lastname}}</td>
									<td>{{$user->email}}</td>



                                    <td><a href="{{ url('/yonetim/kullanici/'.$user->id) }}"><button class="btn btn-primary"><i class="fa fa-edit"></i> Düzenle</button></a>
											<button class="btn btn-danger silbtn" onclick='sil("users","{{$user->id}}","{{$user->email}}")'><i class="fa fa-delete"></i> Sil</button></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
@endsection
@section('sayfajs')
    <script>
        $(function () {
            $('#example2').DataTable({
               "order": [[ 1, "asc" ]],
              "paging": true,
              "lengthChange": true,
              "searching": true,
              "ordering": true,
              "info": false,
              "autoWidth": false,
              "language": {
                "lengthMenu": "Sayfada gösterilecek kayıt sayısı _MENU_",
                "zeroRecords": "Veri bulunamadı",
                "search": "Arama",
                paginate: {
                    first:      "İlk",
                    previous:   "Önceki",
                    next:       "Sonraki",
                    last:       "Son"
                  },
                }
            });
        });
        function sil(tablo,id,email){
            swal({
              title: 'Silmek istediğinize emin misiniz?',
              text: email + ' silinecek ve bir daha geri alamayacaksınız. Onaylıyor musunuz?',
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Sil!",
              cancelButtonText: "Kapat",
              closeOnConfirm: false
            },
            function(){
                $.ajax({
                    type: "POST",
                    url: "{{url('/yonetim/AjaxSil')}}", //process to mail
                    data: '_token={{csrf_token()}}&tablo='+tablo+'&id='+id,
                    success: function(msg){
                        swal({
                          title: "Silindi",
                           text: msg,
                            type: "success"
                          },
                          function(){
                            location.reload();
                        });
                    },
                    error: function(){
                        alert("failure");
                    }
              	});

            });
        }
    </script>
@endsection
