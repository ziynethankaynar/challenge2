@extends('layouts.admin')
@section('sayfacss')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css" type="text/css" rel="stylesheet" />
    <link href="{{ url('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/switchery.min.css') }}" rel="stylesheet">
@endsection
@section('title')
    Y繹netim Ayarlar覺
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-body">
                        @if(session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                     @foreach($user as $users)
                        <form action="" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Email:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="email" class="form-control" value="{{$users->email}}" />
                                </div>
                            </div>

							<div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Name:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control" value="{{$users->name}}" />
                                </div>
                            </div>

							<div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Lastname:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="surname" class="form-control" value="{{$users->surname}}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Tel:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="phone" class="form-control" value="{{$users->phone}}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Başlık:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="title" class="form-control" value="{{$users->title}}" />
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success">Kaydet</button>
                            </div>
                        </form>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
