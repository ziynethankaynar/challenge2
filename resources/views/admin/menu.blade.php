<aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">MENÜ</li>

        <li class="{{ Request::is('yonetim/kullanicilar') ? 'active' : '' }}">
          <a href="{{ url('/yonetim/kullanicilar') }}">
            <i class="fa fa-user"></i> <span>Kullanıcılar</span>
          </a>
        </li>

        <li class="{{ Request::is('yonetim/kitaplar') ? 'active' : '' }}">
          <a href="{{ url('/yonetim/companies') }}">
            <i class="fa fa-book"></i> <span>Şirketler</span>
          </a>
        </li>

		<li class="{{ Request::is('yonetim/yorumlar') ? 'active' : '' }}">
          <a href="{{ url('/yonetim/addresses') }}">
            <i class="fa fa-comment"></i> <span>Adresler</span>
          </a>
        </li>



        <li>
          <a href="{{ url('/yonetim/cikis') }}">
            <i class="fa fa-power-off"></i> <span>Çıkış</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
