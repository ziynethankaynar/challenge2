<style>
img {
  border: 2px solid #ddd;
  border-radius: 4px;
  padding: 5px;
}

img:hover {
  box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}
</style>
<a target="_blank" href="data:image/png;base64,{{ $data }}">
<img src="data:image/png;base64,{{ $data }}"/>
</a>
@foreach($company as $companies)
<?php echo $company->html; ?>
@endforeach
