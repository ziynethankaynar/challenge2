@extends('layouts.app_u')

@section('content')
<div class="container">
 <div class="panel panel-default">
    <div class="panel-heading">Bilgileri Düzenle</div>

<section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-body">

                        @if(session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif


                        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}


                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Ad:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" class="form-control" value="{{$user->name}}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Soyad:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="lastname" class="form-control" value="{{$user->lastname}}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Email:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="email" class="form-control" value="{{$user->email}}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Telefon:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="phone" class="form-control" value="{{$user->phone}}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Başlık:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="title" class="form-control" value="{{$user->title}}" />
                                </div>
                            </div>


                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success">Kaydet</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </section>
   </div> </div>


@endsection
