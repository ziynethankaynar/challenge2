<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title')</title>
        <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/jQueryUI/jquery-ui.css') }}">
        <link href="{{ url('css/AdminLTE.min.css') }}" rel="stylesheet">
        <link href="{{ url('css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/datatables/dataTables.bootstrap.css') }}">
        <!-- Morris chart -->
        <link rel="stylesheet" href="{{ url('plugins/morris/morris.css') }}">
        <link href="{{ url('css/sablon.min.css') }}" rel="stylesheet">
        <link href="{{ url('css/sweetalert.css') }}" rel="stylesheet">
        <link href="{{ url('css/stil.css') }}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
        @yield('sayfacss')
    </head>
    <body class="hold-transition sidebar-mini skin-blue">
        <div class="wrapper">
            @yield('content')
        </div>
        <script src="{{ url('js/jquery-2.2.3.min.js') }}"></script>
        <script src="{{ url('js/bootstrap.min.js') }}"></script>
        <script src="{{ url('js/sweetalert.min.js') }}"></script>
        <script src="{{ url('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
        <script src="{{ url('plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ url('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ url('js/app.min.js') }}"></script>
        <script src="{{ url('js/jquery.oLoader.min.js') }}"></script>
        <script src="{{ url('js/custom.js') }}"></script>
        <script src="{{ url('plugins/noti/notiModal.min.js') }}"></script>
        <script src="{{ url('plugins/jquery.playSound/jquery.playSound.js') }}"></script>
        <script src="{{ url('plugins/morris/raphael.js') }}"></script>
        <script src="{{ url('plugins/morris/morris.min.js')}}"></script>
        <script language="javascript" src="http://ir.sitekodlari.com/yukaricik13.js"></script>
        @yield('sayfajs')
    </body>
</html>
