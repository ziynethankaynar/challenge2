<!DOCTYPE html>
<html>
    <head>
        <title>Yönetim Paneli</title>
        <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ url('css/AdminLTE.min.css') }}" rel="stylesheet">
        <link href="{{ url('css/sablon.min.css') }}" rel="stylesheet">
    </head>
    <body class="sidebar-mini skin-black">
        <div class="login-box">
          <div class="login-logo">
              <b>Yönetim Paneli</b>
          </div>
          <div class="login-box-body">

            @if(count($errors) > 0)
            <div class="row">
                <div class="col-sm-12 alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
            <form action="" method="post">
              <div class="form-group has-feedback">
                <input type="email" class="form-control" placeholder="Email" name="email">
              </div>
              <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Şifre" name="password">
              </div>
              <div class="row">
                <div class="col-xs-8">
                    <!--
                  <div class="checkbox icheck">
                    <label>
                      <input type="checkbox"> Remember Me
                    </label>
                  </div>
                  -->
                </div>
                <div class="col-xs-4">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Giriş</button>
                </div>
              </div>
              {{ csrf_field() }}
            </form>
          </div>
        </div>
        <script src="{{ url('js/jquery-2.2.3.min.js') }}"></script>
        <script src="{{ url('js/bootstrap.min.js') }}"></script>
        <script src="{{ url('js/app.min.js') }}"></script>
    </body>
</html>
