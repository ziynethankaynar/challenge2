<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Spatie\Browsershot\Browsershot;

use App\Company;
use App\Models\User;
use App\Address;
use DB;
use File;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
     public function duzenle($id)
     {
        $user = User::where('id',$id)->first();
        return view('duzenle',compact('user'));
     }
     public function duzenleKayit($id, Request $req)
    {
        $user = User::where('id',$id)->first();

         $user->name = $req->name;
         $user->lastname = $req->lastname;
         $user->email = $req->email;
         $user->phone = $req->phone;
         $user->title = $req->title;

        if($user->save()){
            return redirect()->back()->with('success', 'İşlem başarılı.');
        }else{
            return redirect()->back()->withError('İşlem sırasında bir hata oluştu.');
        }

           $user->save();
        return view('duzenle',compact('user'));
    }

    public function ayril($company_id,$user_id)
    {
       User::where('id',$user_id)->update(['company_id'=>null]);
       $companies = Company::orderBy('name', 'ASC')->get();
       return view('home',compact('companies'));

    }
    public function kayit($id,$user_id)
    {
      $company = Company::find($id);
      $user = User::find($user_id);
      $yeni_user = new User();
      $yeni_user->name = $user->name;
      $yeni_user->lastname = $user->lastname;
      $yeni_user->email = $user->email;
      $yeni_user->password = $user->password;
      $yeni_user->phone = $user->phone;
      $yeni_user->title = $user->title;


      DB::table('users')->where('id', $user->id)->delete();
      $company->user()->save($yeni_user);
      return redirect()->back()->with('success', 'İşlem başarılı.');
    }


   public function detail($id)
   {
     $user = User::where('id', $id)->first();
     $company_id = $user->company_id;
     $companies = Company::where('id',$company_id)->get();
     $addresses = Address::where('company_id',$company_id)->get();
     return view('detail',compact('companies','addresses'));
   }

   public function adres($company_id)
   {
     return view('adres');
   }

   public function adresKayit($company_id,Request $req)
   {
     $company = Company::find($company_id);
     $yeni_adres = new Address();
     $yeni_adres->title = $req->autocomplete;

     $company->adres()->save($yeni_adres);
     return redirect()->back()->with('success', 'Adres başarılı bir şekilde kaydedilmiştir.');
   }

   public function page($company_id)
   {
     $company = Company::where('id',$company_id)->first();
     $text=$company->html;
   $string = $text;
   $font   = 3;
   $width  = 550;
   $height = ImageFontHeight($font);
   $im = @imagecreate ($width,$height);
   $background_color = imagecolorallocate ($im, 255, 255, 255); //white background
   $text_color = imagecolorallocate ($im, 0, 0,0);//black text
   imagestring ($im, $font, 0, 0, $string, $text_color);
   ob_start();
   imagepng($im);

   $imstr = base64_encode(ob_get_clean());
   imagedestroy($im);
     return view('page',compact('company'),array('data'=>$imstr));
   }

    public function logout(Request $req)
   {
     Auth::logout();
     return redirect('/');
   }

}
